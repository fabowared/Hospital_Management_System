<?php

namespace App\Http\Controllers\MobileApplication;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function AddApoiment(Request $request)
    {
        $apo=new Appointment();
        $checkvalid=Appointment::where('date',Carbon::parse($request->date))->where('time',Carbon::parse($request->time))->first();
        if($checkvalid)
        {
            return response()->json(["massege"=>'invalid date and time'], 200);
        }
            $token=json_decode(base64_decode($request->header('token')));
        
            $user=User::where('email',$token->email)->first();
            $checkday=Appointment::where('user_id',$token->id)->where('date',$request->date)->first();
            if($checkday)
            {
                return response()->json(['message'=>'you have pick an apoiment'], 200);
            }
            
            $apo['user_id']=$user->id;
            $name = $user->first_name .' '.$user->middle_name.' '.$user->last_name;
            $apo['department_id']=$request->department_id;
            $apo['date']=Carbon::parse($request->date);
            $apo['time']=Carbon::parse($request->time);
            $checktrusted=Appointment::where('user_id',$token->id)->first();
            if($checktrusted)
            {
                return response()->json(['message'=>'you have already did the appointment'], 200);
            }
            else
            {
                $apo->save();
                return response()->json(['Done'=>$apo,'full name'=>$name], 200);
            }
    }

    public function GetAvailable(Request $request, $day)
{
    $day = Carbon::parse($request->day);
    
    if ($day->isFriday()) {
        return response()->json(['message' => 'We do not work on Fridays'], 200);
    }

    $appointments = [];
    $start = Carbon::createFromTimeString("08:00:00");
    $end = Carbon::createFromTimeString("15:00:00");
    $interval = 15; // Interval in minutes

    while ($start->lessThan($end)) {
        $currentTime = $start->format('H:i:s');
        $existingAppointment = Appointment::whereDate('date', $day->toDateString())
            ->whereTime('time', $currentTime)
            ->first();

        if (!$existingAppointment) {
            $appointments[] = $currentTime;
        }

        // Increment the time by the interval
        $start->addMinutes($interval);
    }

    return response()->json(['available_times' => $appointments], 200);
}

    public function DeleteAppointment(Request $request,)
    {
        $tokens=base64_decode($request->header('token'));
        $tokend=json_decode($tokens);
        $user=User::where('email',$tokend->email)->first();
        $apo=Appointment::where('id',$request->Appointment_id)->first();
            if(!$apo)
            {
                return response()->json(['message'=>'Appointment not found '],200);
            }
                if($user->id == $apo->user_id)
                {
                    $apo->delete();
                    return response()->json(['message'=>'Appointment deleted successfuly'],200);
                }
                else
                {
                    return response()->json(['message'=>'not auth'],200);
                }
            }
}