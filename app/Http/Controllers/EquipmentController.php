<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Equipment;

class EquipmentController extends Controller
{
    public function index()
    {
        $equipment = Equipment::all();
        return response()->json(['All Equipment'=>$equipment],200);
    }

    public function store(Request $request)
    {
        $equipment = Equipment::create($request->all());
        return response()->json(['New Equipment'=>$equipment], 201);
    }

    public function show($id)
    {
        $equipment = Equipment::findOrFail($id);
        return response()->json(['This Equipment'=>$equipment],201);
    }

    public function update(Request $request)
    {
        $equipment = Equipment::findOrFail($request->id);
        if(!$equipment) return response()->json(['Message'=>'wrong id!'], 200);
        $equipment->update($request->all());
        return response()->json(['The Equipment Update '=>$equipment], 200);
    }

    public function destroy($id)
    {
        Equipment::destroy($id);
        return response()->json(['message'=>'the equipment deleted successfully!']);
    }
}
