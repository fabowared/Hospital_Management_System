<?php

namespace App\Http\Controllers;

use App\Models\BelongToDep;
use App\Models\Patient;
use Illuminate\Http\Request;
use App\Models\Death;
use App\Models\Empatient;
use App\Models\EMPBelongTo;

class DeathController extends Controller
{
    // Fetch all death records
    public function index()
    {
        $deaths = Death::all();
        return response()->json(['All Deaths'=>$deaths],200);
    }

    public function getByDate(Request $request)
    {
        $date = $request->input('date');

        $deaths = Death::where('death_date', $date)->get();
        if(!$deaths)
        {
            return response()->json(['message'=>'there is no deaths in this date !']);
        }

        return response()->json(['Deaths By Day'=>$deaths]);
    }

    // Insert a new death record
    public function store(Request $request)
    {
        $patient = Patient::where('id',$request->patient_id)->first();
        if(!$patient){
            return response()->json(["message"=>"Not Exist"]);
        }
        $bel = BelongToDep::where('patient_id' , $patient->id)->first();
        $death = new Death();
        $death->name = $patient->full_name;
        $death->father_name ="";
        $death->mom_name =$patient->mom_name;
        $death->birth_date =$patient->date_of_birth;
        $death->city =$patient->address;
        $death->National_id =$patient->chain;
        $death->death_date = $request->input('death_date');
        $death->death_hour = $request->input('death_hour');
        $death->reason_of_death = $request->input('reason_of_death');
        $death->save();
//        $patient->delete();
//        $bel->delete();

        return response()->json(['The new Death'=>$death ,'Message'=>'The new Death Stored Successfully!'],200);
    }


    public function emstore(Request $request)
    {
        $patient = Empatient::where('id',$request->patient_id)->first();
        if(!$patient){
            return response()->json(["message"=>"Not Exist"]);
        }
        $bel = EMPBelongTo::where('patient_id' , $patient->id)->first();
        $death = new Death();
        $death->name = $patient->full_name;
        $death->father_name ="";
        $death->mom_name =$patient->mom_name;
        $death->birth_date =$patient->date_of_birth;
        $death->city =$patient->address;
        $death->National_id =$patient->chain;
        $death->death_date = $request->input('death_date');
        $death->death_hour = $request->input('death_hour');
        $death->reason_of_death = $request->input('reason_of_death');
        $death->save();
//        $patient->delete();
//        $bel->delete();

        return response()->json(['The new Death'=>$death ,'Message'=>'The new Death Stored Successfully!'],200);
    }

    // Update an existing death record
    public function update(Request $request)
    {
        $death = Death::find($request->id);
//        dd($death);
        if($request->input('name')) $death->name = $request->input('name');
        if($request->input('father_name')) $death->father_name = $request->input('father_name');
        if($request->input('mom_name')) $death->mom_name = $request->input('mom_name');
        if($request->input('birth_date')) $death->birth_date = $request->input('birth_date');
        if($request->input('city')) $death->city = $request->input('city');
        if($request->input('National_id')) $death->National_id = $request->input('National_id');
        if($request->input('death_date')) $death->death_date = $request->input('death_date');
        if($request->input('death_hour')) $death->death_hour = $request->input('death_hour');
        if($request->input('reason_of_death')) $death->reason_of_death = $request->input('reason_of_death');
        $death->save();

        return response()->json(['The Death update is'=>$death],200);
    }

    // Delete a death record
    public function destroy(Request $request)
    {
        $death = Death::find($request->id);
        $death->delete();

        return response()->json(['message' => 'Death record deleted'],200);
    }
}
