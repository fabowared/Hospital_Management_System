<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Resource;

class ResourceController extends Controller
{
    public function index()
    {
        $resources = Resource::all();
        return response()->json(['All Resource'=>$resources],200);
    }

    public function store(Request $request)
    {
        $resource = Resource::create($request->all());
        return response()->json(['New Resource'=>$resource], 200);
    }

    public function show(Request $req)
    {
        $resource = Resource::findOrFail($req->id);
        return response()->json(['This Resource'=>$resource],200);
    }

    public function update(Request $request)
    {
        $resource = Resource::find($request->id);
        if(!$resource)  return response()->json(['Message'=>'wrong id!'], 200);
        $resource->update($request->all());
        return response()->json(['The Resource Update'=>$resource], 200);
    }

    public function destroy(Request $req)
    {
        Resource::destroy($req->id);
        return response()->json(['Message'=>'Resource Deleted successfully!'], 200);
    }
}
