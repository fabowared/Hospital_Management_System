<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Birth;
use Carbon\Carbon;

class BirthController extends Controller
{
    // Fetch all birth records
    public function index()
    {
        $births = Birth::all();
        return response()->json(['All Birth'=>$births],200);
    }

    // Fetch birth records for a specific date
    public function getByDate(Request $request)
    {
        $births = Birth::whereDate('birth_date', Carbon::parse($request->date))->get();
        if($births->isEmpty())
        {
            return response()->json(['message'=>'there is no Birth in this date !']);
        }

        return response()->json(['Births By Day'=>$births],200);
    }

    // Insert a new birth record
    public function store(Request $request)
    {
        $birth = new Birth();
        $birth->name = $request->input('name');
        $birth->father_name = $request->input('father_name');
        $birth->mother_name = $request->input('mother_name');
        $birth->birth_date = Carbon::parse($request->birth_date);
        $birth->city = $request->input('city');
        $birth->national_id = $request->input('national_id');
        $birth->save();

        return response()->json(['The new Birth'=>$birth ,'Message'=>'The new Birth Stored Successfully!'],200);
    }

    // Update an existing birth record
    public function update(Request $request)
    {
        $birth = Birth::find($request->id);
        if($request->input('name')) $birth->name = $request->input('name');
        if($request->input('father_name')) $birth->father_name = $request->input('father_name');
        if($request->input('mother_name')) $birth->mother_name = $request->input('mother_name');
        if(Carbon::parse($request->birth_date)) $birth->birth_date = Carbon::parse($request->birth_date);
        if($request->input('city')) $birth->city = $request->input('city');
        if($request->input('national_id')) $birth->national_id = $request->input('national_id');
        $birth->save();

        return response()->json(['The Birth update is'=>$birth],200);
    }

    // Delete a birth record
    public function destroy(Request $req)
    {
        $birth = Birth::find($req->id);
        $birth->delete();

        return response()->json(['message' => 'Birth record deleted'],200);
    }
}
