<?php

namespace App\Http\Controllers;

use App\Models\ActiveToken;
use App\Models\BelongToDep;
use App\Models\Department;
use App\Models\emArchive;
use App\Models\empatient;
use App\Models\EMPBelongTo;
use App\Models\EMPTransfarOperation;
use App\Models\Patient;
use App\Models\Patient_file;
use App\Models\TransfarOperation;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function add_patient(Request $request)
    {
        try{
        $validate = $request->validate([
            'full_name'=>'required',
            'address'  =>'required',
            'date_of_birth'=>'required|date',
            'mom_name'=>'required',
            'chain'=>'required|integer',
            'gender'=>'required',
            'case_description'=>'required',
            'treatment_required'=>'required',
        ]);
        $token=json_decode(base64_decode($request->header('token')));
        $empatient = Empatient::create([
            'full_name'=>$validate['full_name'],
            'address'=>$validate['address'],
            'date_of_birth'=>$validate['date_of_birth'],
            'mom_name'=>$validate['mom_name'],
            'chain'=>$validate['chain'],
            'gender'=>$validate['gender'],
            'case_description'=>$validate['case_description'],
            'treatment_required'=>$validate['treatment_required']
        ]);


        $belong = new EMPBelongTo();
        $belong->patient_id = $empatient->id;
        $belong->dep_id = $token->id;
        $belong->save();
        if($validate['treatment_required'] == 'emergency treatment')
        {
            $result =Patient::create([
                'id' => $empatient['id'],
                'full_name'=>$empatient['full_name'],
                'address'=>$empatient['address'],
                'date_of_birth'=>$empatient['date_of_birth'],
                'mom_name'=>$empatient['mom_name'],
                'chain'=>$empatient['chain'],
                'gender'=>$empatient['gender'],
                'case_description'=>$empatient['case_description'],
                'treatment_required'=>$empatient['treatment_required']
            ]);

            $belongs = new BelongToDep();
            $belongs->patient_id = $result->id;
            $belongs->dep_id = $token->id;
            $belongs->save();

            $attach = Patient_file::create([
                'patient_id'=>$result->id,
                'department_id'=>2,
                'resident'=>'yes',
                'test_result'=>'test',
                'X_ray_result'=>'x-ray',
            ]);
            $patientArchive = EmArchive::create([
                'full_name'=>$validate['full_name'],
                'address'=>$validate['address'],
                'date_of_birth'=>$validate['date_of_birth'],
                'mom_name'=>$validate['mom_name'],
                'chain'=>$validate['chain'],
                'gender'=>$validate['gender'],
                'case_description'=>$validate['case_description'],
                'treatment_required'=>$validate['treatment_required']
            ]);
            $empatient->delete();
            $belong->delete();
        }
    }catch(\Exception $e)
    {
        return response()->json(['message'=>$e->getMessage()]);

    }
        return response()->json(['message'=>'patient info added successfully!'],200);

    }


    public function transfer_empatient_dep(Request $request){

        $tokenun = json_decode(base64_decode(($request->header('token'))));

        $patient = Empatient::where('id',$request->patient_id)->first();

        if(!$patient)

            return response()->json(['message' => 'InValid ID !']);

        $tr = new EMPTransfarOperation();

        $check = EMPBelongTo::where('dep_id', $tokenun->id)

            ->where('patient_id', $patient->id)->first();

        if (!$check) {

            return response()->json(['message' => 'this patient is not in this department so you cannot tranfer him !']);

        }

        $tr->from_dep_id = $check->dep_id;

        $tr->to_dep_id = $request->tr_department;

        $tr->patient_id = $patient->id;

        $check->dep_id = $request->tr_department;

        $check->save();

        $tr->save();

        $pf = Patient_file::where('patient_id', $patient->id)->first();

        return response()->json(['message'=>'patient transfered succesfully'],200);

    }
    public function transfer_patient_dep(Request $request){

            $tokenun = json_decode(base64_decode(($request->header('token'))));

            $patient = Patient::where('id', $request->patient_id)->first();

            $tr = new TransfarOperation();

            $check = BelongToDep::where('dep_id', $tokenun->id)

                ->where('patient_id', $patient->id)->first();

            if (!$check) {

                return response()->json(['message' => 'this patient is not in this department so you cannot tranfer him !']);

            }

            $tr->from_dep_id = $check->dep_id;

            $tr->to_dep_id = $request->tr_department;

            $tr->patient_id = $patient->id;

            $check->dep_id = $request->tr_department;

            $check->save();

            $tr->save();

            $pf = Patient_file::where('patient_id', $patient->id)->first();

            if ($pf) {

                $pf->department_id = $request->tr_department;

                $pf->save();

            }

        return response()->json(['message'=>'patient transfered succesfully'],200);

    }

    public function Update_EMP_Info(Request $request){

        $tokenun = json_decode(base64_decode(($request->header('token'))));

        $patient = Empatient::where('id', $request->patient_id)->first();

    if(!$patient)

        return response()->json(['message' => 'InValid ID!']);

    if(!EMPBelongTo::where('patient_id',$patient->id)->where('dep_id',$tokenun->id)->first())

        return response()->json(['message' => 'InValid ID!']);

        $patient -> case_description = $request -> case_description;

        $patient -> treatment_required = $request -> treatment_required;

       return response()->json(['Message'=>'Done!'],200);

    }

    public function Update_Info(Request $request){

        $tokenun = json_decode(base64_decode(($request->header('token'))));

        $patient = Patient::where('id', $request->patient_id)->first();

        if(!$patient)
            return response()->json(['message' => 'InValid ID!']);

        if(!BelongToDep::where('patient_id',$patient->id)->where('dep_id',$tokenun->id)->first())
            return response()->json(['message' => 'InValid ID!']);

        $patient -> case_description = $request -> case_description;

        $patient -> treatment_required = $request -> treatment_required;

        return response()->json(['Message'=>'Done!'],200);

    }
    public function patient_file(Request $request)
    {
        $patient_info = patient::where('id',$request->patient_id)->first();


        if(!$patient_info)
        {

            return response()->json(['message'=>'this patient is not exist in the system']);

        }

        $tr = TransfarOperation::where('patient_id', $request->patient_id)->first();


        $belong = BelongToDep::where('patient_id', $request->patient_id)->first();

        $from = Department::where('id', $tr->from_dep_id)->first();

        $belong = Department::where('id', $belong->dep_id)->first();
        $patient_file = Patient_file::where('patient_id',$request->patient_id)->first();

        $data =['patient_info'=>$patient_info,'dep'=>$belong->name,'last_department' =>$from->name , 'other'=>$patient_file];

        return response()->json(['Data'=>$data],200);
    }


    public function test_result(Request $request)
    {

        $patient_file = Patient_file::where('patient_id',$request->patient_id)->where('department_id',$request->department_id)->first();
        if(!$patient_file)

        {

            return response()->json(['message'=>'this patient file is not exist !']);

        }

        $update []= Patient_file::where('patient_id',$request->patient_id)->where('department_id',$request->department_id)->update([

            'test_result'=>$request->test_result,

        ]);

        return response()->json(['message'=>'Test result attached successfully!'],200);

    }


    public function em_test_result(Request $request)
    {
        $patient = empatient::where('id',$request->id)->value('full_name');
        return response()->json(['message'=>' عزيزي المواطن' .$patient. ' يمكنك استلام تحاليلك الطارئة على شكل ورقيات'],200);
    }



    public function X_ray_result (Request $request)
    {

        $patient_file = Patient_file::where('patient_id',$request->patient_id)->where('department_id',$request->department_id)->first();

        if(!$patient_file)

        {

            return response()->json(['message'=>'this patient file not exist!']);

        }

        $update[] = Patient_file::where('patient_id',$request->patient_id)->where('department_id',$request->department_id)->update([

            'X_ray_result'=>$request->X_ray_result,

        ]);

        return response()->json(['message'=>'X-ray attached successfully!'],200);

    }


    public function em_X_ray_result(Request $request)
    {
        $patient = empatient::where('id',$request->id)->value('full_name');
        return response()->json(['message'=>' عزيزي المواطن '.$patient.' يمكنك استلام الصورة الشعاعية الخاصة بك مطبوعة' ],200);
    }



    public function searchbypatient(Request $request)

    {

        $patient_name = $request->input('patient_name');

        $search = Empatient::where('full_name','LIKE','%'.$patient_name.'%')->get();


        return response()->json(['Search'=>$search],200);

    }

    public function all_empatient(Request $request)
    {
        $token = json_decode(base64_decode($request->header('token')));
        $all = EMPBelongTo::where('dep_id',$token->id)->get();
        $response = [];
        foreach($all as $item){
            $patient = empatient::where('id' , $item->patient_id)->first();
            $response[] = $patient;
        }
        return response()->json(['All Emergency Patient'=>$response],200);
    }


}
