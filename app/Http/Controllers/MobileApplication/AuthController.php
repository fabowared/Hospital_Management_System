<?php

namespace App\Http\Controllers\MobileApplication;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request){
        try{
        $validate = $request->validate([
            "first_name"=>"required",
            "middle_name"=>"required",
            "last_name"=>"required",
            "email"=>"required|email",
            "password"=> "required|min:8",
            "phone"=>"required|min:10|max:10",
            "location" => "required"
        ]);
        $email_check=User::where('email',$request['email'])->first();
        if($email_check)
        {
            return response()->json(['message'=>'invalid email'],401);
        }
        $user=new User();
        $user->email=$request->email;
        $user->Password=Hash::make($request->password);
        $user->frist_name=$request->first_name;
        $user->middle_name=$request->middle_name;
        $user->last_name=$request->last_name;
        $user->phone=$request->phone;
        $user->location=$request->location;
        $user->save();
    }catch(\Exception $e)
    {
        return response()->json($e->getMessage());
    }
        return response()->json(['message'=>'User added successfully'],200);

    }
    public function login(Request $request)
    {
        try{
        $validate = $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);
        $user = User::where('email',$request->email)->first();
        if(!$user)
        {
            return response()->json(['message'=>"you have to signup first"],200);
        }
        if(!Hash::check($validate['password'],$user->password))
        {
            return response()->json(['message'=>'Wrong pssword! Try again']);
        }


        $tokenjson = json_encode($user);
        $token = base64_encode($tokenjson);
        $newtoken = new UserToken();
        $newtoken->user_id = $user->id;
        $newtoken->token = $token;
        $check=UserToken::where('token',$token)->first();
            if($check)
            {
                return response()->json(['message'=>'you already loged in']);
            }
            $newtoken->save();
        }catch(\Exception $e)
        {
            return response()->json($e->getMessage());
        }
            return response()->json(['Message'=>'login successfully','token'=>$token],200,['token'=>$token]);
    }

    public function logout(Request $request)
    {
        $token = json_decode(base64_decode($request->header('token')));
        $token = UserToken::where('user_id',$token->id)->delete();
        return response()->json(['message'=>'Logged Out !']);
    }

    public function EditProfile(Request $request)
    {
        $tokend=json_decode(base64_decode($request->header('token')));
            $user1=User::where('email',$tokend->email)->first();
            if($request->phone)
            {
                $user1->phone=$request->phone;
            }
            if($request->location)
            {
                $user1->location=$request->location;
            }
            if($request->password)
            {
                $user1->password=$request->password;
            }
            $user1->save();
            return response()->json(['message'=>'done!'], 200);
        }

}
